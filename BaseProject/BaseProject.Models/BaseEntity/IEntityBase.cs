﻿namespace BaseProject.Models.BaseEntity
{
    public interface IEntityBase
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}