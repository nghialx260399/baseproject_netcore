﻿namespace BaseProject.Models.BaseEntity
{
    public class EntityBase : IEntityBase
    {
        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}