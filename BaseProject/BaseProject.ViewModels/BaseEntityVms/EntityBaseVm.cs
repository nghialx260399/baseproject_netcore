﻿namespace BaseProject.ViewModels.BaseEntityVms
{
    public class EntityBaseVm : IEntityBaseVm
    {
        public bool IsDeleted { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime UpdatedOn { get; set; }
    }
}