﻿using BaseProject.Models.BaseEntity;
using Microsoft.EntityFrameworkCore.Query;
using System.Linq.Expressions;

namespace BaseProject.Data.Infrastructures
{
    public interface IGenericRepository<TEntity> where TEntity : class, IEntityBase
    {
        Task<IList<TEntity>> GetAll(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>>? orderBy = null, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);

        Task<TEntity> GetById(params object[] keyValues);

        Task Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(params object[] keyValues);

        Task<IList<TEntity>> Find(Expression<Func<TEntity, bool>> condition, Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>>? includes = null);
    }
}