﻿namespace BaseProject.Data.Infrastructures
{
    public interface IUnitOfWork : IDisposable
    {
        //IColorReporitory ColorRepository { get; }

        ApplicationDbContext ApplicationDbContext { get; }

        Task<int> SaveChanges();
    }
}