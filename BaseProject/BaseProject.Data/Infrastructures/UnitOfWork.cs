﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseProject.Data.Infrastructures
{
    public class UnitOfWork: IUnitOfWork
    {
        private readonly ApplicationDbContext context;
        //private IColorReporitory colortRepository;

        public UnitOfWork(ApplicationDbContext context)
        {
            this.context = context;
        }

        public ApplicationDbContext ApplicationDbContext => this.context;

        //public IColorReporitory ColorRepository => this.colortRepository ??= new ColorRepository(this.context);

        public void Dispose()
        {
            this.context.Dispose();
        }

        public async Task<int> SaveChanges()
        {
            return await this.context.SaveChangesAsync();
        }
    }
}
