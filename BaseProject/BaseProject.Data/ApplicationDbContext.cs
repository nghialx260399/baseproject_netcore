﻿using BaseProject.Models.BaseEntity;
using BaseProject.Models.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BaseProject.Data
{
    public class ApplicationDbContext : IdentityDbContext<AppUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            BeforSaveChanges();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            BeforSaveChanges();
            return base.SaveChangesAsync(cancellationToken);
        }

        private void BeforSaveChanges()
        {
            var entities = ChangeTracker.Entries();
            foreach (var entity in entities)
            {
                var now = DateTime.Now;
                if (entity.Entity is IEntityBase asEntity)
                {
                    if (entity.State == EntityState.Added)
                    {
                        asEntity.CreatedOn = now;
                        asEntity.UpdatedOn = now;
                    }
                    else if (entity.State == EntityState.Modified)
                    {
                        asEntity.UpdatedOn = now;
                    }
                }
            }
        }
    }
}