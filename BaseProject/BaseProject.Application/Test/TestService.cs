﻿using AutoMapper;
using BaseProject.Data.Infrastructures;

namespace BaseProject.Application.Test
{
    public class TestService : ITestService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        //public async Task<bool> Create(CreateProduct createProduct)
        //{
        //    try
        //    {
        //        var product = mapper.Map<Product>(createProduct);

        //        await this.unitOfWork.ProductRepository.Add(product);
        //        await this.unitOfWork.SaveChanges();

        //        return true;
        //    }
        //    catch (Exception)
        //    {
        //        return false;
        //    }
        //}
    }
}